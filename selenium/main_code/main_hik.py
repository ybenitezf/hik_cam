# recordar que ahora se ejecuta python main_hik.py ip_port.json
# idea log: json con datetime de cuando ocurre el suceso

import os
import sys
import json
import shutil
import ftplib
import logging
from time import sleep
from datetime import date
from selenium import webdriver

# implement log
logging.basicConfig(filename='../res/camera_download.log', level=logging.INFO, format='%(levelname)s: %(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


# ftp function to upload
def upload_video(ftp, js_cam, folder):

	logging.info("Comienza subida del video al servidor " + ftp.host)

	video_name = None

	ftp.cwd("/Videos_hik")

	if folder not in ftp.nlst():
		logging.info("Se crea carpeta " + folder)
		ftp.mkd(folder)

	ftp.cwd(folder)

	name = js_cam["camera"]["ip"]+"_"+str(js_cam["camera"]["port"])+"_"+str(js_cam["camera"]["channel_record"])
	file_list = os.listdir(js_cam["save_to"]+"/"+folder)

	for file_name in file_list:
		if name in file_name:
			video_name = file_name
			break

	if video_name == None:
		logging.critical("No se ubica archivo correspondiente a la camara \"" + name + "\" en la carpeta " + folder)
		return

	path_video = js_cam["save_to"]+"/"+folder+"/"+video_name

	try:
		ftp.storbinary("STOR " + video_name, open(path_video, "rb"))
		logging.info(video_name + " subido con exito al servidor")
		os.remove(path_video)
		logging.info(video_name + " removido de Windows")
		# os.chdir(path)

	except Exception as e:

		try:
			shutil.move(path_video, js_cam["save_to"]+"/"+folder+"/Failed")
		except Exception as ex:
			logging.critical("No se pudo mover el archivo " + video_name + ". Exception: " + str(ex))

		logging.critical("No se pudo transferir el archivo " + video_name + ". Exception: " + str(e))


def get_video_hikvision(path_cameras = '../res/cameras', json_file = None):

	if json_file == None:
		logging.warning("No se especifico desde que camara se requiere grabar")

	web = webdriver.Ie("../driver/IEDriverServer_32bits.exe")

	web.get("file:///C:\\Users\\Administrador\\Desktop\\hik_cam\\selenium\\demo\\en\\demo.html")

	# ingresar por parametros el nombre del json de la camara

	# Asumir que si nos dan path_cameras=None entonces json_file
	# es el camino absoluto al archivo json. En caso contrario el archivo
	# json debe estar en la carpeta por defecto '../res/cameras/'
	if path_cameras is None:
		# json_file es el camino absoluto al archivo
		js = json.load(open(json_file))
	else:
		# el archivo debe estar en ../res/cameras
		js = json.load(
			open(os.path.join(
				os.path.dirname(os.path.realpath(__file__)),
				path_cameras,
				json_file
			))
		)

	# log error load or open json


	try:
		# verificamos que nos ubicamos en la pagina que debemos
		assert "Hikvision" in web.title
	except AssertionError:
		print "This isn't the demo"
		logging.critical("No se pudo abrir la aplicacion. AssertionError.")
		web.close()
		#os.system("taskkill /im IEDriverServer_32bits.exe /F")
		# cuando ocurre una excepcion, no se cierra correctamente
		# asi que matamos el proceso. Si este se cerro con anterioridad, no pasa
		# nada

	ip_input = web.find_element_by_id("loginip")
	user_input = web.find_element_by_id("username")
	port_input = web.find_element_by_id("port")
	pass_input = web.find_element_by_id("password")
	login_btn = web.find_element_by_id("login_btn")
	logout_btn = web.find_element_by_id("logout_btn")
	basicInfo_btn = web.find_element_by_id("basicInfo_btn")
	recordPath = web.find_element_by_id("recordPath")
	downloadPath = web.find_element_by_id("downloadPath")
	startPreview_btn = web.find_element_by_id("startPreview_btn")
	startRecording_btn = web.find_element_by_id("startRecording_btn")
	stopRecording_btn = web.find_element_by_id("stopRecording_btn")
	get_btn = web.find_element_by_id("get_btn")
	set_btn = web.find_element_by_id("set_btn")


	# clear previous data if there is data
	actual_camera = js["camera"]
	ip_input.clear()
	user_input.clear()
	port_input.clear()
	pass_input.clear()
	recordPath.clear()
	downloadPath.clear()

	# set the data
	ip_input.send_keys(actual_camera["ip"].encode())
	user_input.send_keys(actual_camera["user"].encode())
	port_input.send_keys(actual_camera["port"])
	pass_input.send_keys(actual_camera["pwd"].encode())

	recordPath.send_keys(js["save_to"].encode())
	downloadPath.send_keys(js["save_to"].encode())
	set_btn.send_keys("\n")

	login_btn.send_keys("\n")  # click not working... weird

	# it takes some seconds to connect
	sleep(2)

	startPreview_btn.send_keys("\n")
	# takes some time too
	sleep(4)

	# start actions
	startRecording_btn.send_keys("\n")
	logging.info("Comienzo de grabacion de la camara " + js["camera"]["ip"] + "_" + str(js["camera"]["port"]))
	today = date.today()
	sleep(float(js["camera"]["time_in_sec"]))
	stopRecording_btn.send_keys("\n")
	logging.info("Termino de grabacion de la camara " + js["camera"]["ip"] + "_" + str(js["camera"]["port"]))


	logout_btn.send_keys("\n")
	ip_input.clear()
	user_input.clear()
	port_input.clear()
	pass_input.clear()
	web.close()
	#os.system("taskkill /im IEDriverServer_32bits.exe /F")

	# si se mata IEDriver, mueren todas las ventanas de IE
	# si se deslogea grabando o se sale del IE, se corta el video
	sleep(2.5)


	# begin ftp upload
	js_server = json.load(open("../res/server_dir.json"))

	try:
		ftp = ftplib.FTP(js_server["ftp"]["url"].encode())
		ftp.login(js_server["ftp"]["user"].encode(), js_server["ftp"]["pwd"].encode())
	except Exception as e:
		logging.critical("Error de conexion ftp con el servidor " + js_server["ftp"]["url"] + ". Exception: " + str(e))


	upload_video(ftp, js, today.__str__())

	ftp.close()



# http://effbot.org/librarybook/ftplib.htm
# https://docs.python.org/2/library/ftplib.html
# allow activex: https://social.technet.microsoft.com/Forums/ie/en-US/7f8e1bc1-f4be-4274-840c-73f1aab092b2/allow-blocked-content-javascript-on-local-html-files-ie11?forum=ieitprocurrentver
# set IE zoom => 100%
