from ctypes import *
from ctypes.wintypes import DWORD, BYTE, LONG

class NET_DVR_TIME(Structure):
	_fields_ = [
		("dwYear", DWORD),
		("dwMonth", DWORD),
		("dwDay", DWORD),
		("dwHour", DWORD),
		("dwMinute", DWORD),
		("dwSecond", DWORD),
	]


class NET_DVR_PLAYCOND(Structure):
	_fields_ = [
		("dwChannel", DWORD),
		("struStartTime", NET_DVR_TIME),
		("struStopTime", NET_DVR_TIME),
		("byDrawFrame", BYTE),
		("byRes", BYTE*63),
	]


class NET_DVR_GetFileByTime_V40(Structure):
	_fields_ = [
		("lUserID", LONG),
		("sSavedFileName", c_char_p),
		("pDownloadCond", POINTER(NET_DVR_PLAYCOND)),
	]


class NET_DVR_STREAM_INFO(Structure):
	_fields_ = [
		("dwSize", DWORD),
		("byID", BYTE*32),
		("dwChannel", DWORD),
		("byRes", BYTE*32)
	]


class NET_DVR_MANUAL_RECORD_PARA(Structure):
	_fields_ = [
		("struStreamInfo", POINTER(NET_DVR_STREAM_INFO)),
		("lRecordType", LONG),
		("byRes", BYTE*32)
	]






